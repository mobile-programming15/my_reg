import 'package:flutter/material.dart';
import 'package:my_reg/constants.dart';

class StudentName extends StatelessWidget {
  const StudentName ({Key? key, required this.studenName}) : super(key: key);
  final String studenName;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'สวัสดี',
          style: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(
            fontWeight: FontWeight.w200,
          ),
        ),
        Text(
          studenName,
          style: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(
            fontWeight: FontWeight.w200,
          ),
        ),
      ],
    );
  }
}
class StudentClass extends StatelessWidget {
  const StudentClass({Key? key, required this.studentClass}) : super(key: key);
  final String studentClass;

  @override
  Widget build(BuildContext context) {
    return    Text(
      'ชั้นปีที่ 3 คณะวิทยาการสารสนเทศ \n สาขาวิทนาการคอมพิวเตอร์',
      style:
      Theme.of(context).textTheme.subtitle2!.copyWith(
        fontSize: 14.0,
      ),
    );
  }
}

class StudenYear extends StatelessWidget {
  const StudenYear({Key? key, required this.studentyear}) : super(key: key);

  final String studentyear;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 20,
      decoration: BoxDecoration(
        color: kOtherColor,
        borderRadius:
        BorderRadius.circular(kDefaultPadding),
      ),
      child: Center(
        child: Text(
          studentyear,
          style: TextStyle(
              fontSize: 12.0,
              color: kTextBlackColor,
              fontWeight: FontWeight.w200),
        ),
      ),
    );
  }
}
class StudentPicture extends StatelessWidget {
  const StudentPicture({Key? key, required this.picAddress, required this.onPrees}) : super(key: key);
  final String picAddress;
  final VoidCallback onPrees;

  @override
  Widget build(BuildContext context) {
    return   GestureDetector(
      onTap: () {},
      child: CircleAvatar(
        minRadius: 50.0,
        maxRadius: 50.0,
        backgroundColor: kSecondaryColor,
        backgroundImage:
        AssetImage(picAddress),
      ),
    );
  }
}
class StudentDataCard extends StatelessWidget {
  const StudentDataCard({Key? key, required this.title, required this.value, required this.onPress}) : super(key: key);
  final String title;
  final String value;
  final VoidCallback onPress;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        width: MediaQuery.of(context).size.width / 2.5,
        height: MediaQuery.of(context).size.height / 9,
        decoration: BoxDecoration(
          color: kOtherColor,
          borderRadius:
          BorderRadius.circular(kDefaultPadding),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              title,
              style: Theme.of(context)
                  .textTheme
                  .bodyText2!
                  .copyWith(
                  fontSize: 16.0,
                  color: kTextBlackColor,
                  fontWeight: FontWeight.w800),
            ),
            Text(
              value,
              style: Theme.of(context)
                  .textTheme
                  .subtitle2!
                  .copyWith(
                  fontSize: 25.0,
                  color: kTextBlackColor,
                  fontWeight: FontWeight.w300),
            ),
          ],
        ),
      ),
    );
  }
}





