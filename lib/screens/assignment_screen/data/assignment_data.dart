class AssignmentData{
  final String subjectName;
  final String topicName;
  final String asassignDate;
  final String lastDate;
  final String status;

  AssignmentData(this.subjectName, this.topicName, this.asassignDate, this.lastDate, this.status);

}
List<AssignmentData> assignment = [
  AssignmentData('Software Testing','lab 8','31/01/2023','5/02/2023','Submitted'),
  AssignmentData('Multi Media','-','-','-','Pending'),
  AssignmentData('Mobile Programming1','Midterm Exam','22/01/2023','1/02/2023','Submitted'),
  AssignmentData('OOAD','-','-','-','Pending'),
  AssignmentData('Web Programming','NestJs+TypeORM','30/01/2023','5/02/2023','not Submitted'),



];