import 'package:flutter/material.dart';
import 'package:my_reg/constants.dart';
import 'package:device_preview/device_preview.dart';

import '../login_screen/login_screen.dart';
class SplashScreen extends StatelessWidget {
  static String routeName = 'SplashScreen';
  @override
  Widget build(BuildContext context){
    
    Future.delayed(Duration(seconds: 5),(){
      Navigator.pushNamedAndRemoveUntil(context, LoginScreen.routeName, (route) => false);
    });
    
    
    return Scaffold(
      body:  Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('My Reg',style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: kTextWhiteColor,
                  fontSize: 40.0,
                  fontStyle: FontStyle.italic,
                  letterSpacing:2.0,
                ),)
              ],
            ),
            Image.asset('assets/images/buu_logo.png',height: 150.0,
              width: 150.0,)
          ],
        ),
      ),
    );
  }
}