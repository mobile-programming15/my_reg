import 'dart:js';

import 'package:flutter/cupertino.dart';
import 'package:my_reg/screens/splash_screen/splash_screen.dart';
import 'screens/assignment_screen/assignment_screen.dart';

import 'screens/home_screen/home_screen.dart';
import 'screens/login_screen/login_screen.dart';
import 'screens/my_profile/my_profile.dart';

Map<String,WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  LoginScreen.routeName: (context) => LoginScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  MyProfileScreen.routeName: (context) => MyProfileScreen(),
  AssignmentScreen.routeName: (context) => AssignmentScreen(),


};